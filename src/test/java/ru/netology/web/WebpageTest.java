package ru.netology.web;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElementsLocatedBy;


class WebpageTest {
    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeAll
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void setupTest() {
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--disable-dev-shm-usage");
//        options.addArguments("--no-sandbox");
//        options.addArguments("--headless");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(40));
    }

    @AfterEach
    void teardown() {
        driver.quit();
        driver = null;
    }


    @Test
    void shouldTestSendingForm() {
        driver.get("https://www.griddynamics.com/");
        driver.findElement(By.className("search-toggle")).click();
        driver.findElement(By.className("form-field-legacy")).sendKeys("QA Automation" + Keys.ENTER);
        wait.until(visibilityOfAllElementsLocatedBy(By.className("search-card__title")));
        int count = driver.findElements(By.className("search-card__title")).size();
        Assertions.assertTrue(count > 0);
        System.out.println("Count is: " + count);
    }
}

